module gitea.com/go-chi/captcha

go 1.21

require (
	gitea.com/go-chi/cache v0.0.0-20210110083709-82c4c9ce2d5e
	github.com/go-chi/chi/v5 v5.0.4
	github.com/smartystreets/goconvey v0.0.0-20190731233626-505e41936337
	github.com/unknwon/com v0.0.0-20190804042917-757f69c95f3e
)

require (
	github.com/gopherjs/gopherjs v0.0.0-20181103185306-d547d1d9531e // indirect
	github.com/jtolds/gls v4.20.0+incompatible // indirect
	github.com/smartystreets/assertions v0.0.0-20190116191733-b6c0e53d7304 // indirect
)
